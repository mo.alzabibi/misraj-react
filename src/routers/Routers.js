import React from "react";
import { Routes, Route } from "react-router-dom";
import Home from "../pages/home";
import UploadFiles from "../pages/Uploadfiles";
import SingleFile from "../pages/home/file";
import Chat from "../pages/chat";
import Charts from "../pages/home/charts";

const Routers = () => {
  return (
  <>

    <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/Uploadfiles" element={<UploadFiles />} />
          <Route path="/file/:id" element={<SingleFile />} />
          <Route path="/chat" element={<Chat />} />
          <Route path="/charts/:id" element={<Charts/>} />
    </Routes>
    </>
  );
};

export default Routers;
