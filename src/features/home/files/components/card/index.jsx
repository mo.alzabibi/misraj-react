import { Stack, Typography, Menu, MenuItem } from "@mui/material";
import InsertDriveFileIcon from '@mui/icons-material/InsertDriveFile';
import { useNavigate } from "react-router-dom";
import { useMemo, useState } from "react";
import DeleteCsvDialog from "../delete";


export default function ExcelCard({ data }) {
    const [anchorEl, setAnchorEl] = useState(null);

    const [isDelete,setIsDelete] = useState(false);

    const navigate = useNavigate();
    const open = Boolean(anchorEl);

    const handleClick = (event) => setAnchorEl(event.currentTarget);
    const handleClose = () => setAnchorEl(null);
    const handleDeleteClose = () => setIsDelete(false);

    const handleDelete = _ => {
        setIsDelete(true)
        handleClose();
    };


    const DeleteDialgo = useMemo(_=>(
        <DeleteCsvDialog open={isDelete} handleClose={handleDeleteClose} id={data?.id} />
    ),[isDelete,data])

    return (
        <>
        {DeleteDialgo}
            <Stack onClick={handleClick} direction={'row'} flexDirection={'column'} alignItems={'center'} sx={{ cursor:'pointer' }} >
                <InsertDriveFileIcon sx={{ color: 'green', fontSize: 150 }} />
                <Typography fontSize={15} fontWeight={550} >{data?.name}</Typography>
            </Stack>
        
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={open}
                onClose={handleClose}
            >
                <MenuItem onClick={() => navigate(`/file/${data?.id}?name=${data?.name}`)}>Table</MenuItem>
                <MenuItem onClick={() => navigate(`/charts/${data?.id}`)}>Charts</MenuItem>
                <MenuItem onClick={handleDelete}>Delete</MenuItem>
            </Menu>
        </>
    );
}