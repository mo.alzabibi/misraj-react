import React from "react";
import CustomDelete from "../../../../../components/@core/delete";
import useDeleteFile from "../../hooks/useDeleteFile";


export default function DeleteCsvDialog({ id, open, handleClose }) {
  const {mutate:deleteFile} = useDeleteFile()

  const handleDeleteAPI = () => {
    deleteFile(id)
    handleClose();
  };


  return (
      <CustomDelete open={open} handleClose={handleClose} handleDeleteAPI={handleDeleteAPI} text={"file"} />
  );
}
