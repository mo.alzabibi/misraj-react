import { useQuery } from '@tanstack/react-query'
import { getfile } from '../../../../api/files';

const useGetFile = (id) => {
  const query = useQuery({
    queryKey: ['files', id],
    queryFn: () => getfile(id),
  });

  return query;
}

export default useGetFile