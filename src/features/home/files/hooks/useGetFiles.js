
import { useQuery } from '@tanstack/react-query'
import { getfiles } from '../../../../api/files'

const useGetFiles = () => {
  const query = useQuery({ queryKey: ['files'], queryFn:getfiles  })

  return query
}

export default useGetFiles
