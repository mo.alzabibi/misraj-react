
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { Deletefile } from '../../../../api/files';
import { showSuccessToastMessage } from '../../../../utils/notifySuccess';
import { showErrorToastMessage } from '../../../../utils/notifyError';

const useDeleteFile = () => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn:Deletefile,
    onSuccess: (data) => {
      queryClient.invalidateQueries("files");
      showSuccessToastMessage(data?.data?.message)
    },
    onError:(error) =>{
      queryClient.invalidateQueries("files");
      showErrorToastMessage(error?.response?.data?.message)
    }
  });
}

export default useDeleteFile

