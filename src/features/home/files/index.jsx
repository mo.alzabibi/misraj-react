import { Box, Button, Grid, Typography } from "@mui/material";
import ExcelCard from "./components/card";
import { Link, useSearchParams } from "react-router-dom";
import { useEffect, useState } from "react";
import Fuse from "fuse.js";
import Filters from "../../../components/filters";

export default function HomeExcelFeature({data,isSuccess}){
    const [params , setParams] = useSearchParams();
    const [Data,setData] = useState(data)
    const search = params.get("search")||"";


    useEffect(() => {
        if (data && data.length > 0) {
            const fuseOptions = {
                keys: Object.keys(data[0]),
                includeScore: true,
                threshold: 0.4, 
            };
            const fuse = new Fuse(data, fuseOptions);
    
            const results = search ? fuse.search(search).map(result => result.item) : data;
            setData(results);
        }
    }, [search, data]);

    return(
        <Grid  container>
            {isSuccess && data?.length!==0?
            <>
            <Grid item xs={12}><Filters setParams={setParams} params={params}/></Grid>
            {Data?.map((val,index)=>{
            return <Grid key={index} item xs={6} sm={2}  >
                <ExcelCard data={val}/>
            </Grid>
        })}
        </>
            :<Box sx={{ display:'flex',justifyContent:'center',width:'100%',alignItems:'center',flexDirection:'column' }} >
                    <Typography variant="h4" mb={1} >No files Found please upload some</Typography>
                    
                    <Link to={'Uploadfiles'} >
                        <Button sx={{ backgroundColor:"#1976D2",color:'#fff',':hover':{color:"black"} }} >
                    <Typography  >
                        Uploadfiles
                        </Typography>
                        </Button>
                    </Link>
            </Box>}

        </Grid>
    )
}