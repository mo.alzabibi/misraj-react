import { Box, Container, Stack, Typography } from "@mui/material";
import CustomDataGrid from "../../../components/@core/dataGrid";
import { useEffect, useMemo, useState } from "react";
import { useLocation, useSearchParams } from "react-router-dom";
import Filters from "../../../components/filters";
import Fuse from 'fuse.js';
import Loading from "../../../components/@core/loading";

export default function SingleFileFeature({ data }) {
    const [Data, setData] = useState(data)
    const [isSearching,setisSearch] = useState(false)
    const [params, setParams] = useSearchParams();

    const search = params.get("search") || "";

    const columns = useMemo(() => data?.length ? Object.keys(data[0]).map(key => ({
        field: key,
        headerName: key.toUpperCase(),
        minWidth: 150,
    })) : [], [data]);

    const location = useLocation();

    function useQuery() {
        return new URLSearchParams(location.search);
    }

    const query = useQuery();
    const name = query.get('name');


    useEffect(() => {
        if (data && data.length > 0) {
            setisSearch(true)
            const fuseOptions = {
                keys: Object.keys(data[0]), 
                includeScore: true,
                threshold: 0.4, 
            };
            const fuse = new Fuse(data, fuseOptions);

            const results = search ? fuse.search(search).map(result => result.item) : data;
            
            setData(results);
            setisSearch(false)
        }
    }, [search, data]);

    return (
        <Container maxWidth='lg' >
        <Stack spacing={2}>
            <Stack direction={{sm:'row'}} spacing={{ xs:2,sm:0 }} alignItems={'center'} justifyContent={'space-between'}>
            <Typography variant="h5" >{name}</Typography>
            <Filters params={params} setParams={setParams} />
            </Stack>
            <Box sx={{ width: '100%', overflow: 'auto', height: '500px' }}>
                  <Box sx={{ maxWidth : {xs:'300px',sm:'690px',md:'100%'} }}> 
            {!isSearching?<CustomDataGrid columns={columns} rows={Data}  height={"100%"} />:<Loading/>}
            </Box>
            </Box>
        </Stack>
        </Container>
    )
} 