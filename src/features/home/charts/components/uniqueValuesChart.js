import { getRandomColor } from "../../../../utils/getRandomColor";

export const UniqueValuesChart = (columns) => {
  const uniqueColumns = (columns||[])?.filter(col => col?.uniqueValues !== null);
  const colors = uniqueColumns?.map(() => getRandomColor());

  return {
    series: [{
      name: 'Unique Values Count',
      data: uniqueColumns?.map(col => col?.uniqueValuesCount || 0),
    }],
    options: {
      chart: {
        type: 'bar',
        height: 350,
      },
      plotOptions: {
        bar: {
          horizontal: true,
          barHeight: '70%',
          distributed: true,
          dataLabels: {
            position: 'top',
          },
        },
      },
      dataLabels: {
        enabled: true,
        formatter: (val, opt) => uniqueColumns[opt?.dataPointIndex]?.name,
        offsetX: 0,
        offsetY: -20,
        style: {
          fontSize: '12px',
          colors: ['#304758'],
          fontWeight: 'bold'
        },
      },
      xaxis: {
        categories: uniqueColumns?.map(col => col?.name),
        labels: {
          show: true,
          style: {
            fontSize: '12px',
            fontWeight: 'bold'
          }
        },
        title: {
          text: 'Unique Values Count',
          style: {
            fontSize: '14px',
            fontWeight: 'bold',
          }
        },
      },
      yaxis: {
        labels: {
          style: {
            fontSize: '12px',
            fontWeight: 'bold',
          }
        }
      },
      grid: {
        borderColor: '#e7e7e7',
        strokeDashArray: 4,
      },
      colors: colors,
      tooltip: {
        y: {
          formatter: (val) => val,
          title: {
            formatter: (seriesName) => seriesName
          }
        }
      }
    },
  };
};