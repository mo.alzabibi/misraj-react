import { getRandomColor } from "../../../../utils/getRandomColor";

export const MaxChart = (columns) => {
  const sumColumns = (columns||[])?.filter(col => col?.max !== null);
  const colors = sumColumns?.map(() => getRandomColor());

  return {
    series: [{
      name: 'Max of Numeric Columns',
      data: sumColumns?.map(col => col?.max || 0),
    }],
    options: {
      chart: {
        type: 'bar',
        height: 350,
      },
      plotOptions: {
        bar: {
          horizontal: true,
          barHeight: '70%',
          distributed: true,
        },
      },
      dataLabels: {
        enabled: true,
        style: {
          fontSize: '12px',
          colors: ['#304758'],
        },
        formatter: (val) => `${val}`
      },
      xaxis: {
        categories: sumColumns?.map(col => col?.name),
        labels: {
          show: true,
          style: {
            fontSize: '12px',
            fontWeight: 'bold'
          }
        },
        title: {
          text: 'Max of Numeric Columns',
          style: {
            fontSize: '14px',
            fontWeight: 'bold',
          }
        },
      },
      yaxis: {
        labels: {
          style: {
            fontSize: '12px',
            fontWeight: 'bold',
          }
        }
      },
      grid: {
        borderColor: '#e7e7e7',
        strokeDashArray: 4,
      },
      colors: colors,
      tooltip: {
        y: {
          formatter: (val) => `${val}`,
          title: {
            formatter: (seriesName) => seriesName
          }
        }
      }
    },
  };
};
