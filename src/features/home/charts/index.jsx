import React from 'react';
import { Box, Divider, Grid, Stack, Typography } from '@mui/material';
import Chart from 'react-apexcharts';
import { UniqueValuesChart } from './components/uniqueValuesChart';
import { SumChart } from './components/sumChart';
import { MeanChart } from './components/meanChart';
import { StdDevChart } from './components/stdDevChart';
import { MinChart } from './components/minChart';
import { MaxChart } from './components/maxChart';
import { MedianChart } from './components/medianChart';

const ChartsFeature = ({ data }) => {
  const { numColumns = 0, numRows = 0, columns = [] } = data || {};

  return (
    <Grid container spacing={4} sx={{ pr: { sm: 4 }, pl: { sm: 4 } }}>
      <Grid item xs={12} sm={6} sx={{ display: 'flex', width: '100%', justifyContent: 'space-between', alignItems: 'start' }}>
        <Stack sx={{ width: '100%' }}>
          <Typography variant="h4" gutterBottom>File Statistics</Typography>
            <Typography variant='h6' >ID:{data?.id}</Typography>
            <Typography variant='h6' >name:{data?.name}</Typography>
            <Typography variant='h6' >DB name:{data?.publicName}</Typography>
            <Typography variant='h6' >Date:{data?.createdAt?.split('T')?.[0]}</Typography>
            <Typography variant='h6' >Time:{data?.createdAt?.split('T')?.[1]?.split('.')?.[0]}</Typography>
          <Typography variant="h6">Number of Columns: {numColumns}</Typography>
          <Typography variant="h6">Number of Rows: {numRows}</Typography>
        </Stack>
      </Grid>

        <Grid item xs={12} sm={6} sx={{ display: 'flex', width: '100%', justifyContent: 'space-between', alignItems: 'start' }}>

        <Stack sx={{ width: '100%' }}>
          {columns.filter(col => col?.uniqueValues !== null).map(col => (
            <Box sx={{ width: '100%' }} key={col?.name}>
              <Typography variant="subtitle1">{col?.name}</Typography>
              <Typography variant="body2">{col?.uniqueValues?.join(', ')}</Typography>
              <Divider />
            </Box>
          ))}
        </Stack>
      </Grid>
      
      <Grid item xs={12} md={6}>
        <Typography variant="h6" gutterBottom>Unique Values Count</Typography>
        <Chart options={UniqueValuesChart(columns).options} series={UniqueValuesChart(columns).series} type="bar" height={350} />
      </Grid>
      <Grid item xs={12} md={6}>
        <Typography variant="h6" gutterBottom>Sum of Numeric Columns</Typography>
        <Chart options={SumChart(columns).options} series={SumChart(columns).series} type="bar" height={350} />
      </Grid>
      <Grid item xs={12} md={6}>
        <Typography variant="h6" gutterBottom>Mean of Numeric Columns</Typography>
        <Chart options={MeanChart(columns).options} series={MeanChart(columns).series} type="bar" height={350} />
      </Grid>
      <Grid item xs={12} md={6}>
        <Typography variant="h6" gutterBottom>Median of Numeric Columns</Typography>
        <Chart options={MedianChart(columns).options} series={MedianChart(columns).series} type="bar" height={350} />
      </Grid>
      <Grid item xs={12} md={6}>
        <Typography variant="h6" gutterBottom>Standard Deviation of Numeric Columns</Typography>
        <Chart options={StdDevChart(columns).options} series={StdDevChart(columns).series} type="bar" height={350} />
      </Grid>
      <Grid item xs={12} md={6}>
        <Typography variant="h6" gutterBottom>Min of Numeric Columns</Typography>
        <Chart options={MinChart(columns).options} series={MinChart(columns).series} type="bar" height={350} />
      </Grid>
      <Grid item xs={12} md={6}>
        <Typography variant="h6" gutterBottom>Max of Numeric Columns</Typography>
        <Chart options={MaxChart(columns).options} series={MaxChart(columns).series} type="bar" height={350} />
      </Grid>
    </Grid>
  );
};

export default ChartsFeature;