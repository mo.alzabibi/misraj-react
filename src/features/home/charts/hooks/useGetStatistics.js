import { useQuery } from '@tanstack/react-query'
import { getStatistics } from '../../../../api/charts';

const useGetStatistics = (id) => {
  const query = useQuery({
    queryKey: ['files', id],
    queryFn: () => getStatistics(id),
  });

  return query;
}

export default useGetStatistics