import React, { useState, useEffect,useRef } from 'react';
import { Container, Box, Paper, TextField, IconButton, CircularProgress } from '@mui/material';
import SendIcon from '@mui/icons-material/Send';
import usePostChat from './hooks/usePostChats';
import { formateDate } from '../../utils/formatedDate';
const ChatFeature = ({ data,date }) => {
    const [messages, setMessages] = useState([]);
    const today = new Date()

    const [input, setInput] = useState('');

    const {mutate:post,isPending} = usePostChat()
    const chatContainerRef = useRef(null)
    
    useEffect(() => {
        setMessages(data);
        if(messages)
        {
            if(chatContainerRef.current)
                chatContainerRef.current.scrollTop = chatContainerRef.current.scrollHeight;
        }
    }, [data,messages]);

    const handleSend = () => {
        if (input.trim() === '') return;

        const userMessage = { role: 'user', content: input };
        setMessages([...messages, userMessage]);
        setInput('');
        post({prompt:input,conversationId:date})

    };

    return (
        <Container maxWidth="lg" sx={{ height: '80vh', display: 'flex', flexDirection: 'column', width: '100%' }}>
            <Paper ref={chatContainerRef} elevation={3} sx={{ flex: 1, display: 'flex', flexDirection: 'column', p: 2, overflowY: 'auto' }}>
                {messages?.map((msg, index) => (
                    <Box key={index} sx={{ mt: 2, display: 'flex', justifyContent: msg.role === 'user' ? 'flex-end' : 'flex-start' }}>
                        <Box sx={{
                            bgcolor: msg.role === 'user' ? 'primary.main' : 'grey.300',
                            color: msg.role === 'user' ? 'primary.contrastText' : 'black',
                            p: 1,
                            borderRadius: 1,
                            maxWidth: '70%',
                            wordWrap: 'break-word',
                        }}>
                            {msg.content}
                        </Box>
                    </Box>
                ))}
            </Paper>
            {
                date <= formateDate(today)&&
            
            <Box sx={{ display: 'flex', p: 2, alignItems: 'center' }}>
                <TextField
                    value={input}
                    onChange={(e) => setInput(e.target.value)}
                    variant="outlined"
                    fullWidth
                    placeholder="Type your message"
                    onKeyPress={(e) => {
                        if (e.key === 'Enter') handleSend();
                    }}
                />
                
                <IconButton disabled={isPending} color="primary" onClick={handleSend}>
                    <SendIcon />
                </IconButton>
                {
                    isPending && <CircularProgress/>
                }
            </Box>
            }
        </Container>
    );
};

export default ChatFeature;
