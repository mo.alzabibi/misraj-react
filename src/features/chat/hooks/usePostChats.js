
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { postChat } from '../../../api/chats';
import { showErrorToastMessage } from '../../../utils/notifyError';

const usePostChat = () => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn:postChat,
    onSuccess: (data) => {
      queryClient.invalidateQueries("chats");
    },
    onError:(error) =>{
      showErrorToastMessage("try again please")
      queryClient.invalidateQueries("chats");
    }
  });
}

export default usePostChat

