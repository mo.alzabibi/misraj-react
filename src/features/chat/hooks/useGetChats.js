import { useQuery } from '@tanstack/react-query'
import { getChat } from '../../../api/chats';

const useGetChat = (conversationId) => {
  const query = useQuery({
    queryKey: ['chats', conversationId],
    queryFn: () => getChat(conversationId),
  });

  return query;
}

export default useGetChat