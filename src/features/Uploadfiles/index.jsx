import React, { useEffect, useState, useMemo } from 'react';
import { Box, Button, Card, CardContent, CircularProgress, Container, Grid, Stack, Typography } from '@mui/material';
import papa from 'papaparse';
import useUploadFile from './hooks/useUpload';
import { showErrorToastMessage } from '../../utils/notifyError';
import { showSuccessToastMessage } from '../../utils/notifySuccess';
import { useDropzone } from 'react-dropzone';
import CustomDataGrid from '../../components/@core/dataGrid';

export default function FilesFeature() {
  const [files, setFiles] = useState([]);
  const [csvData, setCsvData] = useState([]);
  const [isLoading, setIsLoading] = useState(false); 

  const columns = useMemo(() => csvData?.length ? Object.keys(csvData[0]).map(key => ({
    field: key,
    headerName: key.toUpperCase(),
    minWidth: 150,
  })) : [], [csvData]);

  const { mutate: uploadFile,isPending } = useUploadFile();

  const onDrop = (acceptedFiles) => {
    if (acceptedFiles[0]?.type !== 'text/csv') {
      showErrorToastMessage('Only CSV files are allowed!');
      return;
    }

    setIsLoading(true); 
    setFiles(acceptedFiles);
  };

  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    multiple: false,
    accept: 'text/csv',
    maxSize: 100000000,
  });

  useEffect(() => {
    if (!files.length) return;

    const parseCsvData = (file) => new Promise((resolve, reject) => {
      papa.parse(file, {
        header: true,
        complete: results => resolve(results.data.map((row, index) => ({ ...row, id: index + 1 }))),
        error: reject,
      });
    });

    (async () => {
      try {
        const data = await parseCsvData(files[0]);
        setCsvData(data);
      } catch (error) {
        console.error("Error parsing CSV:", error);
      } finally {
        setIsLoading(false);
      }
    })();

  }, [files]);

  const handleRemove = () => {
    if (csvData.length || files.length) {
      setCsvData([]);
      setFiles([]);
      showSuccessToastMessage("Removed");
    } else {
      showErrorToastMessage("Nothing to remove");
    }
  };

  const handleUpload = () => {
    if (!files[0]) {
      showErrorToastMessage("No file selected");
      return;
    }
    const formData = new FormData();
    formData.append("csvData", files[0]);
    uploadFile(formData);
  };

  return (
    <Container maxWidth="lg">
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Card>
            <CardContent>
              <Stack direction="row" justifyContent="space-between" alignItems="center">
                <Typography variant="h6" color="primary">CSV File:</Typography>
                <Box sx={{ display:'flex',alignItems:'center' }} >
                  <Button onClick={handleRemove} color="error">Remove</Button>
                  <Button disabled={isPending} onClick={handleUpload} variant="contained" color="primary">Upload</Button>
                  {isPending&&<CircularProgress sx={{ ml:1 }} />}
                </Box>
              </Stack>
              <Box sx={{ mt: 3 }}>
                {!csvData.length ? (
                  <Box {...getRootProps()} sx={{ width: '100%', height: '500px', border: '2px dashed gray', p: 2 }}>
                    <input {...getInputProps()} />
                    {isLoading ? (
                      <Typography>Loading......</Typography> 
                    ) : (
                      <Typography>Drag 'n' drop a CSV file here, or click to select files</Typography>
                    )}
                  </Box>
                ) : (
                  <Box sx={{ width: '100%', overflow: 'auto', height: '500px' }}>
                  <Box sx={{ maxWidth : {xs:'300px',sm:'690px',md:'100%'} }}> 
                    <CustomDataGrid rows={csvData} columns={columns} height="100%" />
                  </Box>
                </Box>              
                )}
              </Box>
            </CardContent>
          </Card>

        </Grid>
      </Grid>
    </Container>
  );
}
