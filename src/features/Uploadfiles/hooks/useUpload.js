
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { showSuccessToastMessage } from '../../../utils/notifySuccess';
import { showErrorToastMessage } from '../../../utils/notifyError';
import { postFile } from '../../../api/files';

const useUploadFile = () => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn:postFile,
    onSuccess: (data) => {
      queryClient.invalidateQueries("files");
      showSuccessToastMessage(data?.data?.message)
    },
    onError:(error) =>{
      queryClient.invalidateQueries("files");
      showErrorToastMessage(error?.response?.data?.message)
    }
  });
}

export default useUploadFile

