import axios from "axios";
export const axiosInstance = axios.create({
    baseURL: process.env.REACT_APP_API_BASE_URL,
    headers: {
        Accept: '*/*',
    },
})

axiosInstance.interceptors.response.use(
    (response) => {
        return response
    },

    (error) => {
        return Promise.reject(error);
    }
)