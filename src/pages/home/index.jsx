import Loading from "../../components/@core/loading"
import HomeExcelFeature from "../../features/home/files"
import useGetFiles from "../../features/home/files/hooks/useGetFiles"

export default function Home (){
    const {data,isPending,isSuccess} = useGetFiles()

    return<>
            {!isPending ?
        <HomeExcelFeature data={data?.data?.data} isSuccess={isSuccess}/>

                :
                <Loading />
            }
        </>

}