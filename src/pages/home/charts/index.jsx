import { useParams } from "react-router-dom";
import ChartsFeature from "../../../features/home/charts";
import useGetStatistics from "../../../features/home/charts/hooks/useGetStatistics";
import Loading from "../../../components/@core/loading";

export default function Charts(){
    const { id } = useParams()
    const {data,isPending} = useGetStatistics(id)
    return <>{!isPending?<ChartsFeature data={data?.data?.data}/>:<Loading/>}</>
}