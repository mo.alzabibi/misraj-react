import { useParams } from "react-router-dom";
import SingleFileFeature from "../../../features/home/file";
import useGetFile from "../../../features/home/files/hooks/useGetFile";
import Loading from "../../../components/@core/loading";

export default function SingleFile() {
    const { id } = useParams()  
    const { data, isPending } = useGetFile(id)
    return (
        <>
            {!isPending ?
                <SingleFileFeature data={data?.data?.data} />
                :
                <Loading />
            }
        </>
    )
}