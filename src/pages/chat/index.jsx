import { Box, TextField, Typography } from "@mui/material";
import ChatFeature from "../../features/chat";
import useGetChat from "../../features/chat/hooks/useGetChats";
import { formateDate } from "../../utils/formatedDate";
import { useState } from "react";

export default function Chat(){
    const date = new Date()
    const [filter,setFilter] = useState(formateDate(date))
    const {data} = useGetChat(filter)
    return <>
        <Box sx={{ width:"100%", display:'flex',justifyContent:'end',mb:1,alignItems:'center' }} >
            <Typography variant="h6" mr={1}>Filter</Typography>
            <TextField
            type="date"
            size="small"
            sx={{width:{sm:"30%"}}}
            defaultValue={formateDate(date)}
            onChange={e=>setFilter(formateDate(e.target.value))}
            />
        </Box>
        <ChatFeature data={data?.data?.data} date={filter}/>
    </>
}