import { axiosInstance } from "../../http";

export const getStatistics= (id) => {
    return axiosInstance.get(`file/statistics/${id}`);
  }

