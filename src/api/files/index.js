import { axiosInstance } from "../../http";

export const getfiles = () => {
  return axiosInstance.get('/file');
}


export const postFile = async (file) => {
  return await axiosInstance.post(`/file`, file,

    {
      headers: {
        'Content-Type': 'multipart/form-data',
      }
    }
  )
}


export const getfile = (id) => {
  return axiosInstance.get(`/file/${id}`);
}

export const Deletefile = (id) => {
  return axiosInstance.delete(`/file/delete/${id}`);
}