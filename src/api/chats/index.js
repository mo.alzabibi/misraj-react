import { axiosInstance } from "../../http";

export const getChat = (conversationId) => {
    return axiosInstance.get(`/gpt/chat/${conversationId}`);
  }
export const postChat = (obj) => {
    return axiosInstance.post(`/gpt/chat`,obj);
  }
