import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

let position = "top-right"

export const showSuccessToastMessage = (txt, pos) => {

    if (pos === 'br'){
        position = 'bottom-right'
    }

    if (pos === 'bl'){
        position = 'bottom-left'
    }

    if (pos === 'bc'){
        position = 'bottom-center'
    }

    if (pos === 'tl'){
        position = "top-left"
    }

    if (pos === 'tc'){
        position = "top-center"
    }
    toast.success(`${txt}`, {
    position : pos ? pos : position
    });
  };

