export function formateDate(date) {
    if (isNaN(new Date(date).getTime())) {
      return null;
    }
  
    var originalDate = new Date(date);
    var day = originalDate.getUTCDate();
    var month = originalDate.getUTCMonth() + 1; // Months are zero-based, so we add 1
    var year = originalDate.getUTCFullYear();
  
  
    var formattedDate =year + '-' + (month < 10 ? '0' : '') + month + '-' + day +'';
  
  
    return formattedDate;
  }
  