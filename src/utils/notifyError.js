import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

let position = 'top-right'

export const showErrorToastMessage = (txt, pos) => {

    if (pos === 'br'){
        position = 'bottom-right'
    }

    if (pos === 'bc'){
        position = 'bottom-center'
    }

    if (pos === 'tl'){
        position = "top-left"
    }

    if (pos === 'tr'){
        position = "top-right"
    }

    if (pos === 'tc'){
        position = "top-center"
    }
    toast.error(`${txt}`, {
    position : pos ? pos : position
    });
  };

