import { CssBaseline } from "@mui/material";
import Layout from "./components/Layout/Layout";
import { ToastContainer } from "react-toastify";
import { QueryClientProvider } from "@tanstack/react-query";
import { QueryClient } from '@tanstack/react-query'

const queryClient = new QueryClient()

function App() {
  return<>

    <ToastContainer/>
  
      <CssBaseline />
      <QueryClientProvider client={queryClient}>
  
   <Layout />
   </QueryClientProvider>
  </>
}

export default App;
