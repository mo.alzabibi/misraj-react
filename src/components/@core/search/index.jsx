import { Box, Button, Stack, TextField } from "@mui/material"
import { useFormik } from "formik";
import SearchIcon from '@mui/icons-material/Search';
import RestartAltIcon from '@mui/icons-material/RestartAlt';
const Search = ({ setSearch,value }) => {
    const initialValues = {
        search: value,
    };
    const handleSubmit = (search) => {
        setSearch(search.search)
    };

    const handleReset=_=>{
        setSearch("")
        formik.resetForm()
    }
    const formik = useFormik({
        initialValues,
        onSubmit: search => {
            handleSubmit(search)
        }
    });
    return (
        <form onSubmit={formik.handleSubmit} >
            <Stack direction={'row'} alignItems={'center'} spacing={1} >
                <TextField
                    placeholder="Search"
                    fullWidth
                    name="search"
                    value={formik.values.search}
                    onChange={formik.handleChange}
                    InputProps={{
                        startAdornment: (
                            <Box paddingRight={1}>
                                <svg width='14' height='15' viewBox='0 0 14 15' fill='none' xmlns='http://www.w3.org/2000/svg'>
                                    <g id='zoom-split'>
                                        <path
                                            id='Combined Shape'
                                            fillRule='evenodd'
                                            clipRule='evenodd'
                                            d='M5.75002 11.875C2.64341 11.875 0.125015 9.3566 0.125015 6.25C0.125015 3.1434 2.64341 0.625 5.75002 0.625C8.85662 0.625 11.375 3.1434 11.375 6.25C11.375 9.3566 8.85662 11.875 5.75002 11.875ZM5.75 10.6251C8.16625 10.6251 10.125 8.6663 10.125 6.25005C10.125 3.8338 8.16625 1.87505 5.75 1.87505C3.33376 1.87505 1.375 3.8338 1.375 6.25005C1.375 8.6663 3.33376 10.6251 5.75 10.6251ZM13.692 14.1919C13.936 13.9478 13.936 13.5521 13.692 13.308L11.192 10.808C10.9479 10.5639 10.5522 10.5639 10.3081 10.808C10.064 11.0521 10.064 11.4478 10.3081 11.6919L12.8081 14.1919C13.0522 14.436 13.4479 14.436 13.692 14.1919Z'
                                            fill='#8090A7'
                                        />
                                    </g>
                                </svg>
                            </Box>
                        )
                    }}
                    sx={{ backgroundColor: '#F5F7FA', border: 'none', boxShadow: 'none', width: { sm: '320px', xs: '100%' } }}
                    size='small'
                />
                <Box sx={{ cursor: 'pointer' }} >
                    <Button type="submit" sx={{ color: '#392467' }} >

                        <SearchIcon />
                    </Button>

                    <Button onClick={handleReset} sx={{ color: '#392467' }} >
                        <RestartAltIcon />
                    </Button>
                </Box>
            </Stack>
        </form>
    )
}
export default Search