import { DataGrid } from "@mui/x-data-grid";

export default function CustomDataGrid({ rows, columns, height = '400px' }) {
  return (
    <div style={{ height, width: '100%' }}>
      <DataGrid
        rows={rows || []}
        columns={columns || []}
        rowsPerPageOptions={[5, 10, 20, 50, 100]}  // Options for changing pageSize
        pagination
        autoHeight
        sx={{
          '.MuiDataGrid-columnHeaders': {
            backgroundColor: 'rgba(255, 247, 205, 0.7)',
            fontSize: '0.875rem',
          },
          '.MuiDataGrid-cell': {
            fontSize: '1rem',
          },
        }}
      />
    </div>
  );
}