import React from "react";
import {  Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from "@mui/material";
import CancelRoundedIcon from '@mui/icons-material/CancelRounded';

export default function CustomDelete  ({open,handleClose,handleDeleteAPI,text}){
    return (
            <Dialog onClose={handleClose} open={open}>
                  <DialogContent
                    sx={{
                      height: "99px",
                      width: "100%",
                      borderRadius: "10px",
                      position: "relative",
                      overflow: "visible",
                    }}
                  >
                    <DialogContentText sx={{}}>
                      <CancelRoundedIcon
                        style={{
                          backgroundColor: "#FFFFFF",
                          color: "#A20D29",
                          fontSize: 160,
                          position: "fixed",
                          top: "45%",
                          left: "50%",
                          transform: "translate(-50%, -90%)",
                          borderRadius: "50%",
                        }}
                      />
                    </DialogContentText>
                  </DialogContent>
           
    
                  <DialogTitle style={{ fontSize: "19px", color: "#555" }}>
                    Are you sure you want to delete this {text}?
                  </DialogTitle>
    
                  <DialogActions
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      padding: "10px",
                    }}
                  >
                    <Button onClick={handleClose} style={{ color: "#B4B4B3" }}>
                    Cancel
                    </Button>
                    <Button
                      sx={{ color: "#DF2E38" }}
                      onClick={handleDeleteAPI}
                      autoFocus
                    >
                      Delete
                    </Button>
                  </DialogActions>
            </Dialog>
    );
}

