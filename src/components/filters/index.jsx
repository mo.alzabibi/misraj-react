import {  Stack } from "@mui/material";
import { useCallback, useMemo } from "react";
import Search from "../@core/search";

export default function Filters({setParams,params}){
    


  const handleSearch = useCallback(value => {
    setParams(prevParams => {
      const newParams = new URLSearchParams(prevParams);
      newParams.set("search", value); 
      return newParams;
    });
  }, [setParams]);


    const filters = useMemo(()=>{
        return <>
            <Stack spacing={1} direction={'row'} alignItems={'center'} >
              <Search setSearch={handleSearch} value={params.get("search")} />
            </Stack>
        </>
      },[handleSearch,params])

      return <>
        {filters}
      </>
}

