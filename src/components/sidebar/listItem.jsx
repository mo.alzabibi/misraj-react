import { ListItem, ListItemButton, ListItemText } from "@mui/material";
import { Link } from "react-router-dom";

export default function Listitem({text,link}){
    return (
        <Link to={link} >
            <ListItem disablePadding>
                <ListItemButton>

                    <ListItemText primary={text} />
                </ListItemButton>
            </ListItem>
            </Link>
    )
}