import React from "react";

import Routers from "../../routers/Routers";
import PersistentDrawerLeft from "../sidebar";

const Layout = () => {
  return (
      <PersistentDrawerLeft>
        <Routers />
      </PersistentDrawerLeft>
  );
};

export default Layout;
